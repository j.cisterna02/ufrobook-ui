import { createRouter, createWebHistory } from 'vue-router';
import HomeView from '../views/muro/PaginaPrincipalView.vue';
import CuentaAjustesView from '../views/cuenta/CuentaAjustesView.vue';
import RegisterView from '../views/cuenta/RegisterView.vue';
import LoginView from '../views/cuenta/LoginView.vue';
import RecuperacionEmailView from '../views/cuenta/RecuperacionEmailView.vue';
import VerificarView from '../views/cuenta/validaciones/VerificarView.vue';
import VerificarRecuperarView from '../views/cuenta/validaciones/ValidarRecuperar.vue';
import VerificarEliminacionCuentaView from '../views/cuenta/validaciones/ValidarEliminar.vue';
import MuroView from "../views/muro/MuroView.vue"

const routes = [
  {
    path: '/',
    name: 'home',
    component: HomeView
  },
  {
    path: '/ajustesCuenta',
    name: 'ajustesCuenta',
    component: CuentaAjustesView
  },
  {
    path: '/Login',
    name: 'Login',
    component: LoginView,
    meta: {showNavBar: false}
    
  },
  {
    path: '/Register',
    name: 'Register',
    component: RegisterView,
    meta: {showNavBar: false}
    
  },
  {
    path: '/RecuperacionEmail',
    name: 'RecuperacionEmail',
    component: RecuperacionEmailView,
    meta: {showNavBar: false}
    
  },
  {
    path: '/verificar',
    name: 'verificar',
    component: VerificarView,
    meta: {showNavBar: false}
  },
  {
    path: '/verificar-token-recuperacion',
    name: 'verificar-token-recuperacion',
    component: VerificarRecuperarView,
    meta: {showNavBar: false}
  },
  {
    path: '/eliminar-cuenta',
    name: 'eliminar-cuenta',
    component: VerificarEliminacionCuentaView,
    meta: {showNavBar: false}
  },
  {
    path:"/muro/:id",
    name:"muro",
    component: MuroView
  }
];

const router = createRouter({
  history: createWebHistory(process.env.BASE_URL),
  routes
});



export default router;
