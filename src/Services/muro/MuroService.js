import axios from 'axios'
const BASE_URL = 'http://localhost:3001/'

export const buscarImagenDePerfilDeUnUsuario=async function(usuario){
    try{
        let muro= await axios.get(`${BASE_URL}muro/ver/${usuario.id}`);
        muro=muro.data;
        if(!muro.idFotoPerfil){
            return "https://www.nicepng.com/png/detail/202-2022264_usuario-annimo-usuario-annimo-user-icon-png-transparent.png"
        }else{
            let publicacion = await axios.get(`${BASE_URL}p/publicaciones_id?id_publicacion=${muro.idFotoPerfil}`);
            publicacion=publicacion.data;
            const imagen= await buscarImagenPubli(publicacion.imagenes[0]);
            return imagen;
        }
    }catch(e){
        console.log(e)
        return "https://www.nicepng.com/png/detail/202-2022264_usuario-annimo-usuario-annimo-user-icon-png-transparent.png"
    }
}

export const buscarImagenPubli= async function(nombreimg){
    try {
        const img= await axios.get(`http://localhost:3001/multimedia/fileSystem/img?fileName=${nombreimg}`,{
            responseType: 'blob'
        })
        const blob=img.data;
        const base64= await readBlobAsBase64(blob);

        return base64;
    }catch (e) {
        console.log(e);
    }
}
async function readBlobAsBase64(blob) {
    return new Promise((resolve, reject) => {
        const reader = new FileReader();
        reader.onload = () => {
          const base64 = reader.result;
          resolve(base64);
        };
        reader.onerror = reject;
        reader.readAsDataURL(blob);
      });
  }